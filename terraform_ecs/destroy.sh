if [ -z "$1" ]; then
  echo "[ERROR] you need to pass env name to destroy as argument of this script"
  exit 1
fi
# HANDLE ERROR
set -o errexit -o nounset
#set working directory the current script dirrectoy
cd $(dirname $0)

for d in $(ls -d ./0* | sort -r) ; do
  echo " destroy layer $d";
  cd $d ;
  tf $1 destroy;
  cd -
done
