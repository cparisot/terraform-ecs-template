# NETWORK
variable "aws_region" {
  description = "Region that the instances will be created"
  default     = "eu-west-1"
}

variable "profile" {
  description = "Name of your AWS-Cli profile"
}

variable "availability_zones" {
  type        = "list"
  description = "The azs to use"
  default     = ["1", "2"]
}

variable "environment" {
  description = "The environment"
}

variable "component" {
  default = "api-back"
}

variable "short_component" {
  default = "api"
}

variable "Trigramme" {

}

variable "application" {
  description = "The application name of the app"
}

variable "cluster_database" {
  default     = false
  description = "cluster or unique db"
}

variable "database_name" {
  description = "The database name"
}

variable "database_username" {
  description = "The username of the database"
}

variable "database_password" {
  description = "The password of the database"
}

variable "application_health_check_path" {
  description = "The application health check path"
}

variable "image" {
  description = "The image link of the app"
}

variable "alb_listener_port" {
  description = "port the alb listens on"
  default     = "80"
}

variable "application_image_publish_port" {
  description = "port expose by the image"
  default     = "8080"
}

variable "alb_protocol" {
  description = "alb listener accepted protocol"
  default     = "HTTP"
}

variable "max_capacity" {
  default = "4"
}

variable "ssl_policy" {
  description = "AWS SSL policy"
  default     = "ELBSecurityPolicy-TLS-1-1-2017-01"
}

variable "public_app_domain" {
  description = "The application's public alb DNS"
}
