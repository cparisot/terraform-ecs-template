/* security group for ALB */
resource "aws_security_group" "api_inbound_sg" {
  name        = "${var.Trigramme}-${var.application}-${var.environment}-sg-api-inbound-alb"
  description = "Allow HTTP from VPC and HTTPS from anywhere into ALB, and open AWS admin port"
  vpc_id      = "${data.aws_vpc.vpc.id}"

  ingress {
    from_port   = 80
    to_port     = 4242
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = 443
    to_port     = 4242
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "${var.Trigramme}-${var.application}-${var.environment}-sg-api-inbound-alb"
    Environment = "${var.environment}"
    Component   = "${var.component}"
    Trigramme    = "${var.Trigramme}"
  }
}

/* Security Group for ECS */
resource "aws_security_group" "ecs_service" {
  vpc_id      = "${data.aws_vpc.vpc.id}"
  name        = "${var.Trigramme}-${var.application}-${var.environment}-sg-ecs-service"
  description = "Allow egress from container"

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "${var.Trigramme}-${var.application}-${var.environment}-sg-ecs-service"
    Environment = "${var.environment}"
    Component   = "${var.component}"
    Trigramme    = "${var.Trigramme}"
  }
}
