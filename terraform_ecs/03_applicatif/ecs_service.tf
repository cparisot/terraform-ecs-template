/*====
Cloudwatch Log Group
======*/
resource "aws_cloudwatch_log_group" "logs_api" {
  name = "${var.Trigramme}-${var.application}-${var.environment}-api"
  retention_in_days = "14"
  tags {


    Application = "${var.application}"
    Component   = "${var.component}"
    Environment = "${var.environment}"
    Name        = "${var.Trigramme}-${var.component}-${var.application}-${var.environment}-${var.application}-app"
    Trigramme    = "${var.Trigramme}"
  }
}

/*====
ECS service
======*/

resource "aws_ecs_service" "api" {
  name            = "${var.Trigramme}-${var.application}-${var.environment}-api"
  task_definition = "${aws_ecs_task_definition.api.family}:${max("${aws_ecs_task_definition.api.revision}", "${aws_ecs_task_definition.api.revision}")}"
  desired_count   = 2
  launch_type     = "FARGATE"
  cluster         = "${data.aws_ecs_cluster.ecs_cluster.id}"
  depends_on      = ["aws_iam_role_policy.ecs_service_role_policy"]
  depends_on      = ["aws_ecs_task_definition.api"]
  depends_on      = ["aws_alb.alb_public"]
  network_configuration {
    security_groups = ["${aws_security_group.ecs_service.id}", "${data.aws_security_group.default.id}"]
    subnets         = ["${data.aws_subnet_ids.private_subnet.ids}"]

  }
  load_balancer {
    target_group_arn = "${aws_alb_target_group.alb_api_target_group.arn}"
    container_name   = "${var.application}"
    container_port   = "${var.application_image_publish_port}"
  }


}

resource "aws_alb_target_group" "alb_api_target_group" {
  name        = "${var.Trigramme}-${var.application}-${var.environment}"
  port        = "${var.application_image_publish_port}"
  protocol    = "HTTP"
  vpc_id      = "${data.aws_vpc.vpc.id}"
  target_type = "ip"
  depends_on      = ["aws_alb.alb_public"]
  health_check {
    path     = "${var.application_health_check_path}"
    protocol = "HTTP"
    matcher  = "200"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Application = "${var.application}"
    Component   = "${var.component}"
    Environment = "${var.environment}"
    Name        = "tg-${var.Trigramme}-${var.application}-${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}
