/*====
ECS task definitions
======*/
locals {
  //get correct url depending exitance of cluster or rds
  cluster_url = "${element(concat(data.aws_rds_cluster.cluster_database.*.endpoint, data.aws_db_instance.database.*.address),0)}"
}

locals {
  count        = "${var.cluster_database ? 0 : 1 }"
  //database_url = "${var.cluster_database ? data.aws_db_instance.database.address:""}"
}

/* the task definition for the app service */
data "template_file" "api_task" {
  template = "${file("${path.module}/tasks/api_task_definition.json")}"

  vars {
    imagelocal      = "${var.image}"
    application     = "${var.application}"
    regionlocal     = "${var.aws_region}"
    database_url    = "postgresql://${var.database_username}:${var.database_password}@${local.cluster_url}:5432/${var.database_name}?encoding=utf8&pool=40"
    log_group       = "${aws_cloudwatch_log_group.logs_api.name}"
    environment    = "${var.environment}"
  }
}

resource "aws_ecs_task_definition" "api" {
  family                   = "${var.environment}_api"
  container_definitions    = "${data.template_file.api_task.rendered}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "512"
  memory                   = "1024"
  execution_role_arn       = "${aws_iam_role.ecs_execution_role.arn}"
  task_role_arn            = "${aws_iam_role.ecs_execution_role.arn}"
}
