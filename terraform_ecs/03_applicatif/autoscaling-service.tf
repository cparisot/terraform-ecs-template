/*====
Auto Scaling for ECS
======*/
resource "aws_iam_role" "ecs_autoscale_role" {
  name               = "${var.environment}_ecs_autoscale_role"
  assume_role_policy = "${file("${path.module}/policies/ecs-autoscale-role.json")}"
}
resource "aws_iam_role_policy" "ecs_autoscale_role_policy" {
  name   = "ecs_autoscale_role_policy"
  policy = "${file("${path.module}/policies/ecs-autoscale-role-policy.json")}"
  role   = "${aws_iam_role.ecs_autoscale_role.id}"
}

resource "aws_appautoscaling_target" "target" {
  service_namespace  = "ecs"
  resource_id        = "service/${data.aws_ecs_cluster.ecs_cluster.cluster_name}/${aws_ecs_service.api.name}"
  scalable_dimension = "ecs:service:DesiredCount"
//   role_arn           = "${aws_iam_role.ecs_autoscale_role.arn}"
  min_capacity       = 2
  max_capacity       = "${var.max_capacity}"

  depends_on = ["aws_ecs_service.api"]
}


resource "aws_appautoscaling_policy" "up" {
  name               = "${var.Trigramme}-${var.application}-${var.environment}-scale-up"
  service_namespace  = "ecs"
  resource_id        = "service/${data.aws_ecs_cluster.ecs_cluster.cluster_name}/${aws_ecs_service.api.name}"
  scalable_dimension = "${aws_appautoscaling_target.target.scalable_dimension}"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 15
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = ["aws_appautoscaling_target.target"]
}

resource "aws_appautoscaling_policy" "down" {
  name               = "${var.Trigramme}-${var.application}-${var.environment}-scale-down"
  policy_type        = "StepScaling"
  service_namespace  = "ecs"
  resource_id        = "service/${data.aws_ecs_cluster.ecs_cluster.cluster_name}/${aws_ecs_service.api.name}"
  scalable_dimension = "${aws_appautoscaling_target.target.scalable_dimension}"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 900
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment         = -1
    }
  }

  depends_on = ["aws_appautoscaling_target.target"]
}

# resource "aws_appautoscaling_scheduled_action" "scale" {
#   count              = "${length(var.scheduling)}"
#   name               = "${var.Trigramme}-${var.application}-${var.environment}-schedule-step-${count.index}"
#   service_namespace  = "ecs"
#   resource_id        = "service/${data.aws_ecs_cluster.ecs_cluster.cluster_name}/${aws_ecs_service.api.name}"
#   scalable_dimension = "${aws_appautoscaling_target.target.scalable_dimension}"
#   schedule           = "${lookup(var.scheduling[count.index], "schedule")}"
#
#   scalable_target_action {
#     min_capacity = "${lookup(var.scheduling[count.index], "min_capacity")}"
#     max_capacity = "${lookup(var.scheduling[count.index], "max_capacity")}"
#   }
# }
#
#
# variable "scheduling" {
#   type = "list"
#   default = [
#                      {
#                          schedule = "cron(0 5 ? * 2,3,4,5,6 *)",
#                          min_capacity = 8,
#                          max_capacity = 8,
#                      },
#                      {
#                          schedule = "cron(0 12 ? * 2,3,4,5,6 *)",
#                          min_capacity = 2,
#                          max_capacity = 8,
#                      },
#                  ]
# }


/* metric used for auto scale */
resource "aws_cloudwatch_metric_alarm" "service_cpu_high" {
  alarm_name          = "${var.Trigramme}-${var.application}-${var.environment}-api-cpu-utilization-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "50"

  dimensions {
    ClusterName = "${data.aws_ecs_cluster.ecs_cluster.cluster_name}"
    ServiceName = "${aws_ecs_service.api.name}"
  }

  alarm_actions = ["${aws_appautoscaling_policy.up.arn}"]
  ok_actions    = ["${aws_appautoscaling_policy.down.arn}"]
}
