/*====
App Load Balancer
======*/


resource "aws_alb" "alb_public" {
  name            = "alb-${var.Trigramme}-${var.application}-${var.environment}"
  subnets         = ["${data.aws_subnet_ids.public_subnet.ids}"]
  security_groups = ["${aws_security_group.api_inbound_sg.id}", "${data.aws_security_group.default.id}"]
  tags {


    Component   = "${var.component}"
    Environment = "${var.environment}"
    Name        = "alb-${var.Trigramme}-${var.application}-${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}

resource "aws_alb_listener" "public_listener" {
  load_balancer_arn = "${aws_alb.alb_public.arn}"
  port              = "${var.alb_listener_port}"
  protocol          = "${var.alb_protocol}"
  depends_on        = ["aws_alb_target_group.alb_api_target_group"]

  ssl_policy        = "${var.ssl_policy}"
  certificate_arn   = "${data.aws_acm_certificate.api_certificate.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.alb_api_target_group.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "unsecure_listener" {
  load_balancer_arn = "${aws_alb.alb_public.arn}"
  port              = "80"
  protocol          = "HTTP"
  depends_on        = ["aws_alb_target_group.alb_api_target_group"]
  # default redirect to https
  default_action {
    type = "redirect"
    redirect {
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
