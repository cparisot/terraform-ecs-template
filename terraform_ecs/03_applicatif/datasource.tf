data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.environment}-vpc"]
  }
}

data "aws_subnet_ids" "private_subnet" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  filter {
    name   = "tag:Name"
    values = ["${var.environment}*private*"]
  }
}

data "aws_subnet_ids" "public_subnet" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  filter {
    name   = "tag:Name"
    values = ["${var.environment}*public*"]
  }
}

data "aws_ecs_cluster" "ecs_cluster" {
  cluster_name = "ecs-${var.Trigramme}-${var.application}-${var.environment}-cluster"
}

data "aws_subnet_ids" "subnet_ids" {
  vpc_id = "${data.aws_vpc.vpc.id}"

  filter {
    name   = "tag:Name"
    values = ["${var.environment}*"]
  }
}

data "aws_security_group" "default" {
  name = "${var.Trigramme}-${var.application}-${var.environment}-sg-default"
}

data "aws_db_instance" "database" {
  count                   = "${var.cluster_database ? 0 : 1 }"
  db_instance_identifier = "${var.environment}-database"
}

data "aws_rds_cluster" "cluster_database" {
  count                   = "${var.cluster_database ? 1 : 0 }"
  cluster_identifier      = "cluster-${var.application}-${var.environment}"
}

data "aws_instance" "bastion" {

  filter {
    name   = "tag:Name"
    values = ["${var.environment}-bastion"]
  }
}

data "aws_acm_certificate" "api_certificate" {
  domain   = "${var.public_app_domain}"
  statuses = ["ISSUED"]
}
