data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["${var.environment}-vpc"]
  }
}

data "aws_subnet_ids" "private_subnet" {
  vpc_id = "${data.aws_vpc.vpc.id}"
  filter {
    name   = "tag:Name"
    values = ["${var.environment}*private*"]
  }
}

# NETWORK
data "aws_instance" "bastion" {

  filter {
    name   = "tag:Name"
    values = ["${var.environment}-bastion"]
  }
}
