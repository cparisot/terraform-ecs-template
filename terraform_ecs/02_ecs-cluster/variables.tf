# AWS ACCOUNT INFORMATION
variable "availability_zones" {
  type        = "list"
  description = "The azs to use"
  default     = ["1", "2"]
}

variable "aws_region" {
  description = "Region that the instances will be created"
  default     = "eu-west-1"
}

variable "environment" {
  description = "The environment"
}

variable "profile" {
  description = "Name of your AWS-Cli profile"
}


# APPLICATION INFORMATION
variable "component" {
  default = "api-back"
}

variable "short_component" {
  default = "api"
}

variable "Trigramme" {
  
}

variable "application" {
  description = "The application name of the app"
}
