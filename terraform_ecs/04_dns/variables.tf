# AWS ACCOUNT INFORMATION

variable "aws_region" {
  description = "Region that the instances will be created"
  default     = "eu-west-1"
}

variable "profile" {
  description = "Name of your AWS-Cli profile"
}

variable "availability_zones" {
  type        = "list"
  description = "The azs to use"
  default     = ["1", "2"]
}

variable "environment" {
  description = "The environment"
}
# DNS INFORMATION

variable "hosted_public_zone_id" {
  description = "The application's public hosted zone"
}

variable "public_app_domain" {
  description = "The application's public alb DNS"
}

variable "component" {
  default = "api-back"
}

variable "short_component" {
  default = "api"
}

variable "Trigramme" {
  
}

variable "application" {
  description = "The application name of the app"
}
