//terraform import aws_route53_record.app_public_domain ZYON964E9S6FW_betty.demo.livin.digital.engie.com_A
resource "aws_route53_record" "app_public_domain" {
  zone_id = "${var.hosted_public_zone_id}"
  name    = "${var.public_app_domain}"
  type    = "A"

  alias {
    name                   = "${data.aws_alb.alb_public.dns_name}"
    zone_id                = "${data.aws_alb.alb_public.zone_id}"
    evaluate_target_health = false
  }
}
