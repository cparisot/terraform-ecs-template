#!/bin/bash
set -x
set -e

# Update instance
apt update -y

# install psotgres client ;)
apt-get -y install postgresql postgresql-contrib

echo '${bootstrap_keys}' > /home/ubuntu/.ssh/authorized_keys
echo '${cypa_keys}' >> /home/ubuntu/.ssh/authorized_keys

chown ubuntu: /home/ubuntu/.ssh/authorized_keys
chmod 0600 /home/ubuntu/.ssh/authorized_keys
