resource "aws_key_pair" "auth" {
   key_name   = "bootstrap-${var.environment}"
   public_key = "${file("ssh_keys/bootstrap.pub")}"
}


data "template_file" "autoscaling_user_data" {
  template = "${file("${path.module}/bastion_user_data.tpl")}"

  vars {
    bootstrap_keys           = "${file("ssh_keys/bootstrap.pub")}"
    cypa_keys                = "${file("ssh_keys/cypa.pub")}"
  }
}

resource "aws_instance" "bastion" {
  ami                         = "${data.aws_ami.aws_ami_search.id}"
  instance_type               = "${var.instance_type}"
  user_data                   = "${data.template_file.autoscaling_user_data.rendered}"
  vpc_security_group_ids      = ["${aws_security_group.bastion.id}"]
  subnet_id                   = "${aws_subnet.public_subnet.0.id}"
  key_name                    = "${aws_key_pair.auth.key_name}"

  tags {
    Name        = "${var.environment}-bastion"
    Environment = "${var.environment}"
    Component   = "${var.component}"
    Environment = "${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}
resource "aws_eip" "lb" {
  instance = "${aws_instance.bastion.id}"
  vpc      = true

  tags {
    Name        = "${var.environment}-bastion"
    Environment = "${var.environment}"
    Component   = "${var.component}"
    Environment = "${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}

// security
resource "aws_iam_instance_profile" "ecs_instance_profile" {
  name = "${var.environment}-bastion-instance-profile"
  role = "${aws_iam_role.ecs_instance_role.name}"
}

data "aws_iam_policy_document" "ecs_instance_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecs_instance_role" {
  name   = "${var.environment}-bastion-instance-role-policy"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_instance_role.json}"
}

resource "aws_security_group" "bastion" {
  name        = "${var.environment}-sg-bastion"
  description = "Default security group to allow inbound/outbound from the VPC"
  vpc_id      = "${aws_vpc.vpc.id}"
  depends_on  = ["aws_vpc.vpc"]

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "tcp"
    cidr_blocks = ["${var.bastion_allowed_cidr}"]
  }


  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {


    Component   = "${var.component}"
    Environment = "${var.environment}"
    Name        = "${var.environment}-sg-bastion"
    Trigramme    = "${var.Trigramme}"
  }
}
