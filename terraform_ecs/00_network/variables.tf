variable "vpc_cidr" {
  description = "The CIDR block of the vpc"
  default = "10.0.0.0/16"
}


variable "public_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the public subnet"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "private_subnets_cidr" {
  type        = "list"
  description = "The CIDR block for the private subnet"
  default     = ["10.0.10.0/24", "10.0.20.0/24"]
}

variable "environment" {
  description = "The environment"
}

variable "aws_region" {
  description = "The region to launch infra"
}

variable "availability_zones" {
  type        = "list"
  description = "The az that the resources will be launched"
}


variable "profile" {
  description = "Name of your AWS-Cli profile"
  default     = "default"
}

variable "component" {
  default = "api-back"
}

variable "short_component" {
  default = "api"
}

variable "Trigramme" {

}

# INSTANCE MANAGEMENT
variable "instance_type" {
 default = "t2.micro"
}

# SSH Keys
variable "bootstrap_keys" {
 description = "file listing all authorized ssh keys"
 default = ""
}

variable "cypa_keys" {
 description = "file listing all authorized ssh keys"
 default = ""
}

variable "bastion_allowed_cidr" {
  description = "The CIDR block for bastion access"
  default     = ["62.23.55.220/32", "165.225.76.0/23"]
}
