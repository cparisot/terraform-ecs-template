/* subnet used by rds */
resource "aws_db_subnet_group" "rds_subnet_group" {
  name        = "${var.environment}-rds-subnet-group"
  description = "RDS subnet group"
  subnet_ids = ["${data.aws_subnet_ids.private_subnet.ids}"]
  tags {
    
    
    Component   = "${var.component}"
    Environment = "${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}
