/*====
RDS mono db
======*/
resource "aws_rds_cluster" "default" {
  count                   = "${var.cluster_database ? 1 : 0 }"
  cluster_identifier      = "cluster-${var.application}-${var.environment}"
  db_subnet_group_name    = "${aws_db_subnet_group.rds_subnet_group.name}"
  database_name           = "${var.database_name}"
  master_username         = "${var.database_username}"
  master_password         = "${var.database_password}"
  storage_encrypted       = false
  backup_retention_period = "${var.retention_period}"
  engine                  = "aurora-postgresql"
  engine_version          = "10.6"
  skip_final_snapshot     = true
  //enabled_cloudwatch_logs_exports = ["audit"]
  vpc_security_group_ids  = ["${aws_security_group.default_database.id}"]

}

resource "aws_rds_cluster_instance" "cluster_writer" {

  count                      = "${var.cluster_database ? 2 : 0 }"
  cluster_identifier         = "cluster-${var.application}-${var.environment}"
  identifier                 = "cluster-${var.application}-${var.environment}-${count.index}"
  instance_class             = "${var.cluster_database_instance_class}"
  engine                     = "aurora-postgresql"
  apply_immediately          = true
  promotion_tier             = "1"
  db_parameter_group_name    = "default.aurora-postgresql10"
  auto_minor_version_upgrade = false
  depends_on        = ["aws_rds_cluster.default"]

  tags {


    Component   = "${var.component}"
    Environment = "${var.environment}"
    Name        = "cluster-${var.Trigramme}-${var.application}-${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}
