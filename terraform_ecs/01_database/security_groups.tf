resource "aws_security_group" "default_database" {
  name   = "${var.Trigramme}-${var.application}-${var.environment}-sg-database"
  vpc_id = "${data.aws_vpc.vpc.id}"

  ingress {
    from_port   = "5432"
    to_port     = "5432"
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_vpc.vpc.cidr_block}"]
  }

  ingress {
    from_port   = "5432"
    to_port     = "5432"
    protocol    = "tcp"
    cidr_blocks = ["${data.aws_instance.bastion.public_ip}/32"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    
    
    Component   = "${var.component}"
    Environment = "${var.environment}"
    Name        = "${var.Trigramme}-${var.application}-${var.environment}-sg-database"
    Trigramme    = "${var.Trigramme}"
  }
}
