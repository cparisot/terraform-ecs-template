# NETWORK
variable "aws_region" {
  description = "Region that the instances will be created"
  default     = "eu-west-1"
}

variable "profile" {
  description = "Name of your AWS-Cli profile"
}


variable "environment" {
  description = "The environment"
}

variable "component" {
  default = "api-back"
}

variable "short_component" {
  default = "api"
}

variable "Trigramme" {
  
}

variable "application" {
  description = "The application name of the app"
}

//DATABASE
variable "allocated_storage" {
  default     = "20"
  description = "The storage size in GB"
}

variable "instance_class" {
  description = "The instance type"
}

variable "cluster_database_instance_class" {
  description = "The instance type"
  default = "db.r4.large"
}

variable "multi_az" {
  default     = false
  description = "Muti-az allowed?"
}

variable "cluster_database" {
  default     = false
  description = "cluster or unique db"
}

variable "database_name" {
  description = "The database name"
}

variable "database_username" {
  description = "The username of the database"
}

variable "database_password" {
  description = "The password of the database"
}

variable "retention_period" {
  type        = "string"
  default     = "5"
  description = "Number of days to retain backups for"
}

variable "database_logs_retention_in_days" {
  type        = "string"
  default     = "5"
  description = "Number of days to retain log message"
}
