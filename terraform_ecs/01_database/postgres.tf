/*====
RDS mono db
======*/

resource "aws_db_instance" "rds" {
  count                   = "${var.cluster_database ? 0 : 1 }"
  identifier              = "${var.environment}-database"
  allocated_storage       = "${var.allocated_storage}"
  engine                  = "postgres"
  engine_version          = "10.6"
  instance_class          = "${var.instance_class}"
  multi_az                = "${var.multi_az}"
  // use "gp2" (general purpose SSD) - "standard" is (magnetic)
  storage_type            = "gp2"
  name                    = "${var.database_name}"
  username                = "${var.database_username}"
  password                = "${var.database_password}"
  db_subnet_group_name    = "${aws_db_subnet_group.rds_subnet_group.name}"
  vpc_security_group_ids  = ["${aws_security_group.default_database.id}"]
  copy_tags_to_snapshot   = true
  backup_retention_period = "${var.retention_period}"
  skip_final_snapshot     = true
  enabled_cloudwatch_logs_exports  = ["postgresql"]

  tags {


    Component   = "${var.component}"
    Environment = "${var.environment}"
    Trigramme    = "${var.Trigramme}"
  }
}
