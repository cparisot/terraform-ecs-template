Trigramme           = "cypa"

environment         = "template"
aws_region          = "eu-west-1"
# Profile AWS
profile = "default"

# Network
availability_zones      = ["eu-west-1a", "eu-west-1b"]

/* Database */
// WARNING  use cluster_database= true for prod ready environment
cluster_database   = false
// only needed if cluster_database=false to a replicat in other AZ
multi_az           = false
database_name      = "postgresdb"
database_username  = "postgresUser"
# data password should be set at least in TF_VAR_database_password (in aws_env.sh)
database_password ="YouMustNotUseThisMethodToProvidePassword"
instance_class     = "db.t2.micro"

/* application */
application                     = "demo"
image                           = "cyparisot/side-project:demo"
application_image_publish_port  = "8080"
application_health_check_path   = "/heartbeat"
max_capacity                    = "8"


# alb-listeners 443
alb_protocol      = "HTTPS"
alb_listener_port = "443"

# DNS
hosted_public_zone_id  = "ZLC49SB4CYWRU"
public_app_domain      = "demo.template.bogops.io"
