# Terraform

## How to init new environment/workspace
```
cd terraform_ecs
tf init
tf workspace new dev
```

It will create a _terraform.tfstate.d_ folder inside each folder with your tfstate for each environment



WARNING : Use same name for each *environment*.tfvars and your workspace


## How to select an workspace

To display which workspace you're on

`terraform workspace list`
```
default
* dev
```
To select a different workspace
`
terraform workspace select default
   Switched to workspace "default".
`

## Install a wrapper

Before each of the commands listed below. it will:
- check that you are in the same workspace in all your directories
- check that your environment variables match the workspace you are in

It will also apply your command to all the directories within your current directory

Clone the wrapper project: https://github.com/sebiwi/terraform-wrapper
Symlink the `tf.rb` file somewhere in your path, like so:

`ln -s <terraform_wrapper_directory>/tf.rb /usr/local/bin/tf`
Remember to use the full path to your terraform-wrapper’s directory.

Commands will be applied to all directories within current directory:
- tf workspace new <workspace>
- tf workspace select <workspace>
- tf <workspace> plan
- tf <workspace> apply
- tf <workspace> destroy

**WARNING 1**: apply and destroy do not do a plan before applying changes
**WARNING 2**: make sure workspaces and environments <environment>.tfvars share the same name
