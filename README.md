# Infra as code by Terraform

## tl;dr
you want to run your docker image quicky on prod ready-environment:

- Edit env_test/terraform.tfvars for your main custom variables
- Run terraform  (see section "create your infra")
- You're ready to go

## Archi
![Architecture](./img/archi.png)

This teraform is compose of 5 layers
## Layer network
  This folder create a VPC with 2 subnets (1 public and 1 private) in each Availability Zone.
  Keeping our resources in more than one zone is the first thing to do, to achieve high availability.
  If one physical zone fails, our application can answer from the others.

  The private subnet is allowed only to be accessed from resources inside the public network In our case, will be the Load Balancer only and the provide bastion.

## Layers database
  This folder deploy a RDS database or aurora cluster (depending on boolean valiue of cluster_database). It will be located on the private subnet. Allowing only the public subnet to access it.

## Layer ECS (Fargate)
### Fargate (ECS) reminder
    AWS Fargate is a technology for Amazon ECS that allows to run containers without having to manage servers or clusters.

    - **Cluster** : It is a group of EC2 instances hosting containers.
    - **Task definition**: It is the specification of how ECS should run your app. Here you define which image to use, port mapping, memory, environments variables, etc.
    - **Service**: Services launches and maintains tasks running inside the cluster. A Service will auto-recover any stopped tasks keeping the number of tasks running as you specified.

### Layers 02 to 04
    Thess folder provision ECS cluster, create a task definition for your app.
    It create the load balancers on the public subnet and will forward the requests to the ECS service and link it with DNS user friendly.


#### Autoscaling
      This infrastructure handle auto-scaling via metrics in CloudWatch and trigger to scale it up or down.
      If the CPU usage is greater than 85% from 2 periods, we trigger the alarm_action that calls the scale-up policy. If it returns to the Ok state, it will trigger the scale-down policy.

# How to create or update aws infra
## Prerequisites
### AWS cli
  - Install aws cli
  https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
   * Quick install mac :
    `brew install awscli`
   * Quick install ubuntu :
    `sudo apt-get install awscli`
  - Ensure you have enough capacity on your AWS account
  Ensure your AWS credentials are available in your shell
  Customise your credentials via Environment variables:
  https://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html
  adapte the following script [aws_env_exemple.sh](https://gitlab.com/numengie/numengie-infra/blob/master/Requirements/aws_env_exemple.sh) to your need

  - To quickly check your aws cli run :
  `aws sts get-caller-identity`


## Terraform
  Terraform is distributed as a binary package for all supported platforms and architectures.
  **Version used in this project Terraform v0.11.11**
  [(Download here)](https://releases.hashicorp.com/terraform/0.11.11/)


* quick "install" on unix based platforms
copy  _terraform_ binary to to your _/usr/local/bin/_

* To quickly check your terrform :
`terraform -v`

## Wrapper
This wrapper's purpose is to run easly run Terraform on a layered terraform project.
[More information available here.](https://github.com/sebiwi/terraform-wrapper/blob/master/README.md)

- Install ruby https://www.ruby-lang.org/en/documentation/installation/
 * Quick install mac :
  `brew install ruby`
 * Quick install ubuntu :
  `sudo apt-get install ruby-full`

- Clone the wrapper project: https://github.com/sebiwi/terraform-wrapper
Symlink the `tf.rb` file somewhere in your path, like so:

 `ln -s <terraform_wrapper_directory>/tf.rb /usr/local/bin/tf
`


## Create your infra

1. First, you have to create a aws_env file with your secret information:

  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY

	```
	source ../../aws_env.sh
	```
	*check env_test/terraform.tfvars for main custom variables*

  test AWS connection
  ```
  aws sts get-caller-identity
  ```
2. Install a wrapper

  Before each of the commands listed below. it will:
  - check that you are in the same workspace in all your directories
  - check that your environment variables match the workspace you are in

  It will also apply your command to all the directories within your current directory

  Clone the wrapper project: https://github.com/sebiwi/terraform-wrapper
  Symlink the `tf.rb` file somewhere in your path, like so:

  `ln -s <terraform_wrapper_directory>/tf.rb /usr/local/bin/tf`
  Remember to use the full path to your terraform-wrapper’s directory.

  Commands will be applied to all directories within current directory:
  - tf workspace new <workspace>
  - tf workspace select <workspace>
  - tf <workspace> plan
  - tf <workspace> apply
  - tf <workspace> destroy

  **WARNING 1**: apply and destroy do not do a plan before applying changes
  **WARNING 2**: make sure workspaces and environments <environment>.tfvars share the same name

3. Move to terraform directory

	```
	cd terraform_ecs/
	```

4. Load terraform module and select env specified workspace

	```
	tf init
  tf workspace new <env test>
	```
5. Plannification of ressources

	```
	tf <env test> plan
	```

6. Apply ressources

	```
	tf <env test> apply
	```

## Destroy your infra

  ```
  ./destroy.sh <env test>
  ```

## Pipeline CI

see _gitlab-ci_sample_ folder for sample build deploy gitlab-ci pipeline


## Bastion

  Database is only available though the VPC bastion.

  1. Add the bastion private key:

      ```
      $ ssh-add /terraform_ecs/00_network/ssh_keys/bootstrap
      ```

  2. Enter the passphrase. *OPS4EVER*
      Or best replace by your own one, then don't forget to `tf apply` ;)

  3. Connect to the bastion using ssh :
    - Get IP of your bastion
      ```
      $ export ENV=<name_env>
      $ aws ec2 describe-instances  --filters "Name=tag:Name,Values=$ENV-bastion"   "Name=instance-state-name,Values=running" \
                                    --output text \
                                    --query 'Reservations[0].Instances[0].PublicIpAddress'
      ```
    - Connect with ip
      ```
      $ ssh ubuntu@<bastion-ip> -A
      ```
    - Or with the search
      ```
      ssh ubuntu@$(aws ec2 describe-instances  --filters "Name=tag:Name,Values=*bastion" \
                                               "Name=instance-state-name,Values=running" \
                                               --output text \
                                               --query 'Reservations[0].Instances[0].PublicIpAddress') -A
      ```
      Bastion IPs can be checked here : https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:sort=instanceId

4. Connect to the RDS instance with `psql`:

    ```
    $ psql \
       --host=<databaseurl> \
       --port=5432 \
       --username=<database_username> \
       --password \
       --dbname=<database_name>
    ```

## license

This project is licensed under the WTFPL License - see the LICENSE file for details

![wtfpl](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)
