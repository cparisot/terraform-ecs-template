# Install AWS Command Line Interface
# more details https://aws.amazon.com/cli/
apk add --update python python-dev py-pip
pip install awscli --upgrade

# Set AWS config variables used during the AWS get-login command below
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
export AWS_ACCESS_KEY_ID="$(aws configure get aws_access_key_id --profile default)"
export AWS_SECRET_ACCESS_KEY="$(aws configure get aws_secret_access_key --profile default)"
export AWS_DEFAULT_REGION="$(aws configure get region --profile default)"
export AWS_SESSION_TOKEN="$(aws configure get aws_session_token)"
# Log into AWS docker registry
$(aws ecr get-login --no-include-email --region $AWS_REGION | tr -d '\r')

# build image We will use the $CI_ENVIRONMENT_SLUG (This variable equals the environment).
# thanks to this var, we can use this same script for all of our environments (production and staging ...).
docker build --pull -t $AWS_REGISTRY_IMAGE:$CI_ENVIRONMENT_SLUG .
docker push $AWS_REGISTRY_IMAGE:$CI_ENVIRONMENT_SLUG
