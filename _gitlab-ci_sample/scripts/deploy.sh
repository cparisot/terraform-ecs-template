# Install AWS Command Line Interface
# more details https://aws.amazon.com/cli/
apk add --update python python-dev py-pip
pip install awscli --upgrade


# Set AWS config variables used during the AWS get-login command below
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

#change variable VERSION to first 8 digit
 # Set AWS config variables use for AWS engie SQS service
 sed -e 's/VERSION_TO_REPLACE/'"${CI_COMMIT_SHA:0:8}"'/' \
 -e 's/AWS_ACCESS_KEY_ID_SQS/'"${AWS_ACCESS_KEY_ID_SQS}"'/' \
 -e 's#AWS_SECRET_ACCESS_KEY_SQS#'"${AWS_SECRET_ACCESS_KEY_SQS}"'#' \
  scripts/api-task-definition-"$CI_ENVIRONMENT_SLUG".json > task-definition-temp.json

# Our private AWS registry has our new container
# We need updated task definition of our ECS cluster.
# Note that we give it the JSON file describing our task definition also use with terraform
aws ecs register-task-definition --family "$CI_ENVIRONMENT_SLUG"_api	 --requires-compatibilities FARGATE --cli-input-json file://task-definition-temp.json --region $AWS_REGION

# Tell our service to use the latest version of task definition.
aws ecs update-service --cluster "$CI_ENVIRONMENT_SLUG"-ecs-cluster --service $CI_ENVIRONMENT_SLUG-api --task-definition "$CI_ENVIRONMENT_SLUG"_api --region $AWS_REGION
